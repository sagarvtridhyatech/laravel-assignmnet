@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">

                    <table class="table text-md-center" width="50">
              
                    <tbody>
                   
                     <tr>
                      <td><b>Profile-img:</b></td>
                      <td><img src="{{asset('upload/'.$user['profile'])}}" width="50" /></td>
                     </tr> 

                     <tr>
                       <td><b>Name:</b></td>
                       <td> {{ $user['name'] }}</td>
                        
                     </tr>

                     <tr>
                       <td><b>Email:</b></td>
                       <td> {{ $user['email'] }}</td>
                     </tr>

                     <tr>
                       <td><b>Gender:</b></td>
                       <td> {{ $user['gender'] }}</td>
                     </tr>

                     

                     
                                                                                
                    </tbody>
                  </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
