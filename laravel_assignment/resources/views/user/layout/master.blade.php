<!--

=========================================================
* Now UI Dashboard - v1.5.0
=========================================================

* Product Page: https://www.creative-tim.com/product/now-ui-dashboard
* Copyright 2019 Creative Tim (http://www.creative-tim.com)

* Designed by www.invisionapp.com Coded by www.creative-tim.com

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

-->
<!DOCTYPE html>
<html lang="en">

@include('user.layout.head')


<body class="">
  <div class="wrapper ">
    @include('user.layout.sidebar')
    <div class="main-panel" id="main-panel">
      <!-- Navbar -->
    @include('user.layout.nav')
     
      <!-- End Navbar -->
     
      <div class="content container-fluid mt-4">
     @yield('content')
     </div>
      @include('user.layout.footer')
    </div>
  </div>
  <!--   Core JS Files   -->
    @include('user.layout.script')
  
</body>

</html>