@extends('user.layout.master')

@section('title','Userprofile')
@section('content')
 
       <table class="table text-md-center" width="50">
              
                    <tbody>
                   
                     <tr>
                      <td><b>Profile-img:</b></td>
                      <td><img src="{{asset('upload/'.$users['profile'])}}" width="50" /></td>
                     </tr> 

                     <tr>
                       <td><b>Name:</b></td>
                       <td> {{ $users['name'] }}</td>
                        
                     </tr>

                     <tr>
                       <td><b>Email:</b></td>
                       <td> {{ $users['email'] }}</td>
                     </tr>

                     <tr>
                       <td><b>Gender:</b></td>
                       <td> {{ $users['gender'] }}</td>
                     </tr>

                     <tr>
                       <td><button class="btn btn-dark">
                          <a href="/Updateform/{{ $users['id'] }}">Update</a></button></td>
                     </tr>

                     
                                                                                
                    </tbody>
                  </table>
      
@endsection
