@extends('admin.layout.master')

@section('title','Dashboard')
@section('content')
 
       <table class="table">
              <thead class=" text-primary">
                      <th>Profile-img</th>
                      <th>Name</th>
                      <th>Gender</th>
                      <th>Mobile</th>
                      <th>Email</th>
                      <th>Status</th>
                      <th>Update</th>
                      <th>Delete</th>


                    </thead>
                    <tbody>
                    	@foreach($users as $d)
                      <tr>
                      	
                        <td>
                         <img src="{{asset('upload/'.$d['profile'])}}" width="50" />
                         </td>
                        <td>
                         {{ $d['name'] }}
                        </td>
                        <td>
                         {{ $d['gender'] }}
                        </td>
                        <td>
                         {{ $d['phone'] }}
                        </td>
                        <td>
                         {{ $d['email'] }}
                        </td>
                        <td>
                          @if($d['status']=="active")
                          
                            <button class="btn btn-primary"><a href="/status/{{ $d['id'] }}">active</a></button>
                          
                          @else
                          
                             <button class="btn btn-danger"><a href="/status/{{ $d['id'] }}">Inactive</a></button>
                          
                          @endif

                        </td>
                        <td>
                          <button class="btn btn-dark">
                          <a href="/Updateform/{{ $d['id'] }}">Update</a></button>
                        </td>
                        <td>
                          <button class="btn btn-danger">
                          <a href="/delete/{{ $d['id'] }}">Delete</a></button>
                        </td>
                        
                        

                      </tr>
                      @endforeach
                    </tbody>
                  </table>
      
@endsection
