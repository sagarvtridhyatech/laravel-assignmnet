@extends('admin.layout.master')
@section('title','Register')
@section('content')
<div class="well">
 
    {!! Form::open(['url' => '/Admin', 'class' => 'form-horizontal','enctype'=>'multipart/form-data']) !!}
 
    <fieldset>
 
        <legend>Form</legend>
        
        <div class="form-group">
            {!! Form::label('name', 'Name:', ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-6">
                {!! Form::text('name', $value = null, ['class' => 'form-control', 'placeholder' => 'name']) !!}
                  <p class="text-danger font-weight-bold mt-3">{{ $errors->first('name')}} </p>
                        </div>
        </div>
       
       <div class="form-group">
            {!! Form::label('radios', 'Gender:', ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-6">
                <div>
                    {!! Form::label('male', 'male') !!}
                    {!! Form::radio('radio', 'male',['checked'=>'true']) !!}
 
                
                    {!! Form::label('female', 'female') !!}
                    {!! Form::radio('radio', 'female') !!}
                  

                </div>
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('radios', 'Status:', ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-6">
                <div>
                               
                    {!! Form::label('active', 'active') !!}
                    {!! Form::radio('status', 'active',['checked'=>'true']) !!}

                    {!! Form::label('inactive', 'inactive') !!}
                    {!! Form::radio('status', 'inactive') !!}

                                    
                </div>
            </div>
        </div>
       
        <div> 
            {!! Form::label('profile', 'Profile-img:', ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-6">
                {!! Form::file('profile', $value = null, ['class' => 'form-control', 'placeholder' => 'profile-img']) !!}

                  <p class="text-danger font-weight-bold mt-3">{{ $errors->first('profile')}} </p>
                
            </div>
        </div>
        
        <!-- Email -->
        <div class="form-group">
            {!! Form::label('email', 'Email:', ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-6">
                {!! Form::email('email', $value = null, ['class' => 'form-control', 'placeholder' => 'email']) !!}

                  <p class="text-danger font-weight-bold mt-3">{{ $errors->first('email')}} </p>
               
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('mobile', 'Mobile-no:', ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-6">
                {!! Form::number('mobile', $value = null, ['class' => 'form-control', 'placeholder' => 'Mobile-no']) !!}

                  <p class="text-danger font-weight-bold mt-3">{{ $errors->first('mobile')}} </p>
               
            </div>
        </div>
 
        <!-- Password -->
        <div class="form-group">
            {!! Form::label('password', 'Password:', ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-6">
                {!! Form::password('password',['class' => 'form-control', 'placeholder' => 'Password', 'type' => 'password']) !!}

                  <p class="text-danger font-weight-bold mt-3">{{ $errors->first('password')}} </p>
               
                
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('password', 'Confirm-Password:', ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-6">
                {!! Form::password('password_confirmation',['class' => 'form-control', 'placeholder' => 'Password', 'type' => 'password']) !!}


            </div>
        </div>
       
        
 
        <!-- Submit Button -->
        <div class="form-group">
            <div class="col-lg-10 col-lg-offset-2">
                {!! Form::submit('Submit', ['class' => 'btn btn-lg btn-info pull-center'] ) !!}
            </div>
        </div>
 
    </fieldset>
 
    {!! Form::close()  !!}
 
</div>
@endsection
