@extends('admin.layout.master')

@section('title','Update')
@section('content')
<link rel="../../apple-touch-icon" sizes="76x76" href="img/apple-icon.png">
  <link rel="icon" type="../../image/png" href="img/favicon.png">
  <link href="../../css/bootstrap.min.css" rel="stylesheet" />
  <link href="../../css/now-ui-dashboard.css?v=1.5.0" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="../../demo/demo.css" rel="stylesheet" />
<div class="well">
 
    {!! Form::open(['url' => '/updatedata', 'class' => 'form-horizontal','enctype'=>'multipart/form-data']) !!}
 
    <fieldset>
 
        <legend>Update Form</legend>
            
                {!! Form::hidden('userid', $res['id'], ['class' => 'form-control', 'placeholder' => 'name']) !!}

        <div class="form-group">
            {!! Form::label('name', 'Name:', ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-6">
                {!! Form::text('name', $res['name'], ['class' => 'form-control', 'placeholder' => 'name']) !!}

                  <p class="text-danger font-weight-bold mt-3">{{ $errors->first('name')}} </p>

            </div>
        </div>
       
       <div class="form-group">
            {!! Form::label('radios', 'Gender:', ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-6">
                <div>
                    {!! Form::label('male', 'male') !!}
                    {!! Form::radio('radio', 'male',['checked'=>'true']) !!}
 
                
                    {!! Form::label('female', 'female') !!}
                    {!! Form::radio('radio', 'female') !!}
                </div>
            </div>
        </div>
       
            <img src="{{asset('upload/'.$res['profile'])}}" width="50" />   
        <div> 
            {!! Form::label('profile', 'Profile-img:', ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-6">
                {!! Form::file('profile', ['class' => 'form-control', 'placeholder' => 'profile-img']) !!}

                  <p class="text-danger font-weight-bold mt-3">{{ $errors->first('profile')}} </p>

            </div>
        </div>
        
        <!-- Email -->
        <div class="form-group">
            {!! Form::label('email', 'Email:', ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-6">
                {!! Form::email('email', $res['email'], ['class' => 'form-control', 'placeholder' => 'email']) !!}
                  <p class="text-danger font-weight-bold mt-3">{{ $errors->first('email')}} </p>

            </div>
        </div>

        <div class="form-group">
            {!! Form::label('mobile', 'Mobile-no:', ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-6">
                {!! Form::number('mobile',$res['phone'] , ['class' => 'form-control', 'placeholder' => 'Mobile-no']) !!}

                  <p class="text-danger font-weight-bold mt-3">{{ $errors->first('mobile')}} </p>

            </div>
        </div>
 
        
       
        
 
        <!-- Submit Button -->
        <div class="form-group">
            <div class="col-lg-10 col-lg-offset-2">
                {!! Form::submit('Update', ['class' => 'btn btn-lg btn-info pull-center'] ) !!}
            </div>
        </div>
 
    </fieldset>
 
    {!! Form::close()  !!}
    
 
</div>
@endsection
