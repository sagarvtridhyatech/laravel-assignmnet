<div class="sidebar" data-color="orange">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red | yellow"
        -->
      <div class="logo">
        <a href="http://www.creative-tim.com" class="simple-text logo-mini">
          CT
        </a>
        <a href="http://www.creative-tim.com" class="simple-text logo-normal">
          Creative Tim
        </a>
      </div>
      <div class="sidebar-wrapper" id="sidebar-wrapper">
        <ul class="nav">
          <li>
            <a href="./Admin">
              <i class="now-ui-icons design_app"></i>
              <p>Dashboard</p>
            </a>
          </li>
          <li>
            <a href="/Register">
              <i class="now-ui-icons education_atom"></i>
              <p>Register</p>
            </a>
          </li>
          <li>
            <a href="./Adpro">
              <i class="now-ui-icons ui-1_bell-53"></i>
              <p>AdminProfile</p>
            </a>
          </li>
                    
          
        </ul>
      </div>
    </div>