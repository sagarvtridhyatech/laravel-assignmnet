<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Validator;

class RegisterController extends Controller
{
    public function register(Request $req)
    {

      $validator=Validator::make($req->all(),[
            'name' => 'required|max:30|min:3',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|confirmed|min:6',
            'profile' => 'required',
            'mobile' => 'required|digits:10',
        ]);

     if($validator->fails())
     {
       return redirect('/Register')->withErrors($validator)->withInput();
     }

            


    	$t=time();
		$img = $t.'_'.$req->file('profile')->getClientOriginalName();
       
       $data = new User();
       $data->name = $req->name;
       $data->gender = $req->radio;
       $data->profile = $img;
       $data->phone = $req->mobile;
       $data->email = $req->email;
       $data->status = $req->status;
       $data->password = \Hash::make($req->password);
       $data->save();
       $req->file('profile')->move('upload/',$img);
       return redirect('/Admin');
    }
}
