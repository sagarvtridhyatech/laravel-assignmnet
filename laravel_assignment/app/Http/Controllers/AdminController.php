<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\User;
use App\Admin;
class AdminController extends Controller
{
   public function admin(Request $req)
   {
       $data = new Admin();
       $email = $req->email;
       $password = $req->password;

       $count = Admin::where('email', $email)->where('password', $password)->first();

         

       if($count !='')
       {
       	 $req->Session()->put('adminname',$email);
       	 $req->Session()->put('Adminstatus',true);
           
       return redirect('/Admin');
       }
       else
       {
           return redirect('/adminlogin');

       }
   
            
   } 

   public function user(Request $req)
   {
       $data = new User();
       $email = $req->email;
       $password = $req->password;

       $count = User::where('email', $email)->where('password', $password)->first();

         

       if($count !='')
       {
         $req->Session()->put('adminname',$email);
         $req->Session()->put('Adminstatus',true);
           
       return redirect('/User');
       }
       else
       {
           return redirect('/userlogin');

       }
   
            
   } 

   public function logout()
   {
     Session::flush();
      
   	 return redirect('/adminlogin');
   }
}
