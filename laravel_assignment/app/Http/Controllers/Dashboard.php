<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\User;

class Dashboard extends Controller
{
   	public function index()
   	{

   		$user=new User();

   	  $record=$user->get()->toArray();
      return view('admin.index',['users'=> $record]);
   		
   	}
   	public function register()
   	  
   	{
   		return view('admin.register');
   	}

      public function Admin()
      {
         return view('admin.loginadmin');
      }
      
      public function adminprofile()
      {
         return view('admin.adminprofile');
      }

      public function userindex()
      {

         
         
      }
      
      public function user()
      {
         return view('user.userdata');
      }

      public function useradmin()
      {
         return view('user.loginadmin');
      }
      
      public function useradminprofile(Request $req)
      {
        
         $user=new User();
        $value = $req->session()->get('adminname');
        $data = $user::where('email',$value)->first();        

      
     
         return view('user.adminprofile',['users'=> $data]);
      }
}

