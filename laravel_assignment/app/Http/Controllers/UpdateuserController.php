<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Validator;


class UpdateuserController extends Controller
{

	public function updateform($id,Request $req)  
   	{


   		$user = new User();
   		$res = $user->find($id);
   		return view('admin.update',['res' => $res]);
   	}
    public function status(Request $req)
    {

    	$id=$req->id; 
    	$data= new User();
    	$model = $data::find($id);
    	if($model->status == "active")
    	{
    		$model->status = "inactive";
	    	
    		$model->save();
    		return redirect('/Admin');
    	}
    	else
    	{
    		$model->status = "active";
    		$model->save();
    		return redirect('/Admin');
    	}

    } 

    public function delete(Request $req)
    {
    	$id=$req->id;
    	$data= new User();
    	$data::find($id)->delete();

        return redirect('/Admin');
    }

    public function update(Request $req)
    {
    	$validator=Validator::make($req->all(),[
            'name' => 'required|max:30|min:3',
            'email' => 'required|email',
                 
            'profile' => 'required',
            'mobile' => 'required|digits:10',
        ]);

     if($validator->fails())
     {
       return redirect('/Updateform/{id}')->withErrors($validator)->withInput();
     }

    	$id=$req->userid;
    	$data=new User();

    	
    	$d=$data::find($id);
    	
    	$t=time();
		$img = $t.'_'.$req->file('profile')->getClientOriginalName();
       
       $d->name = $req->name;
       $d->gender = $req->radio;
       $d->profile = $img;
       $d->phone = $req->mobile;
       $d->email = $req->email;
       $d->save();
       $req->file('profile')->move('upload/',$img);
       

         
       return redirect('/Admin');

    }


public function userupdate(Request $req)
    {
      $validator=Validator::make($req->all(),[
            'name' => 'required|max:30|min:3',
            'email' => 'required|email',
                 
            'profile' => 'required',
            'mobile' => 'required|digits:10',
        ]);

     if($validator->fails())
     {
       return redirect('/Updateform/{id}')->withErrors($validator)->withInput();
     }

      $id=$req->userid;
      $data=new User();

      
      $d=$data::find($id);
      
      $t=time();
    $img = $t.'_'.$req->file('profile')->getClientOriginalName();
       
       $d->name = $req->name;
       $d->gender = $req->radio;
       $d->profile = $img;
       $d->phone = $req->mobile;
       $d->email = $req->email;
       $d->save();
       $req->file('profile')->move('upload/',$img);
       

         
       return redirect('/User');

    }
}
