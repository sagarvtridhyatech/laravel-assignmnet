<?php

namespace App\Http\Middleware;

use Closure;
use Session;
class Admincheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      
            if(! Session::has('Adminstatus'))
            {
                return redirect('/adminlogin');
            }
        
        return $next($request);
    }
}
