<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/adminlogin', 'Dashboard@Admin');
Route::post('/adminlogin_check', 'AdminController@admin');
Route::group(['middleware'=>'Admincheck'],function()
{
	Route::get('/Register', 'Dashboard@Register');
	Route::get('/Adpro', 'Dashboard@adminprofile');
	Route::get('/User', 'Dashboard@user');
	Route::get('/userlogin', 'Dashboard@useradmin');
	Route::get('/UserAdpro', 'Dashboard@useradminprofile');
	Route::get('/Admin', 'Dashboard@index');
	Route::get('/delete/{id}','UpdateuserController@delete');
	Route::get('/status/{id}','UpdateuserController@status');
	Route::get('/Updateform/{id}', 'UpdateuserController@updateform');
	Route::get('/admin_logout', 'AdminController@logout');
});
Route::post('/userlogindata', 'AdminController@user');

Route::post('/updatedata', 'UpdateuserController@update');
Route::post('/userupdatedata', 'UpdateuserController@userupdate');



Route::get('/user_logout', 'AdminController@logout');




//Route::post('/Admin', 'RegisterController@register');



Auth::routes();

Route::get('/', 'HomeController@index')->name('auth');
